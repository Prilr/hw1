package hw1;

import java.io.*;
import java.util.*;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.filecache.DistributedCache;

import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;

import org.apache.hadoop.util.*;

public class HW1 extends Configured implements Tool
{
	// Mapper class.
	public static class MapClass extends Mapper<IntWritable,Text,Text,IntWritable>
	{
		// We don't need to know about the bid amount, only the quantity.
		// Therefore, the mapper will output records with values of 1.
		private final static IntWritable one = new IntWritable(1);
		// Minimal bid amount to track.
		private final static IntWritable bidTreshold = new IntWritable(250);
        private HashMap<Integer, String> cityNames = new HashMap<Integer, String>();

		// Load the city ID-Name map cached file.
		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			try {
				// Get the list of cache files.
				Path[] cityNames = DistributedCache.getLocalCacheFiles(context.getConfiguration());
				// Assuming there's only one path in the list, as it should be:
				if(cityNames != null && cityNames.length == 1) {
					for(Path cityNamesFile : cityNames) {
						// Read data into the DistributedCache.
						readFile(cityNamesFile);
					}
				}
			} catch(IOException ex) {
				System.err.println("Exception in mapper setup: " + ex.getMessage());
			}
		}
	
		// Load the cache file.
		private void readFile(Path filePath) {
			try {
				BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath.toString()));
				String cityName = null;
				// Read until EOF.
				while((cityName = bufferedReader.readLine()) != null) {
					// String elements split on tab.
					String[] cstr = cityName.split("\t");
					// First element - ID, second - city name.
					cityNames.put(Integer.parseInt(cstr[0]), cstr[1]);
				}
			} catch(IOException ex) {
				System.err.println("Exception while reading city names file: " + ex.getMessage());
			}
		}
	
		public void map(IntWritable key, Text value, Context context)
		{
			try {
				// Fields are separated by tab characters.
				String[] split_str = value.toString().split("\t");

				// Pair: city ID and bid amount.
				// Bid amount is in col. 20, city ID is in col. 8.
				IntWritable str = new IntWritable(Integer.parseInt(split_str[7]));
				IntWritable bid = new IntWritable(Integer.parseInt(split_str[19]));

				// Map to an output pair only if the bid is >250.
				// Otherwise there's no output record.
				if (bid.get() > 250) {
					// Convert city ID to name through the cache file mapping.
					context.write(new Text(cityNames.get(str.get()).toString()), one);
				}
			}
		catch(Exception e) {
			System.out.println(e.getMessage());
			}
		}
	}
   
   // Reducer/Combiner class.
   public static class ReduceClass extends Reducer<Text,IntWritable,Text,IntWritable>
	{
		public void reduce(Text key, Iterable <IntWritable> values, Context context) throws IOException, InterruptedException
		{
			// Calculate the amount of high-value bids per city.
			int sum = 0;
			// Values themselves are unnessessary - we're interested in their amount.
			for (IntWritable val : values)
			{
				sum += 1;
			}
			context.write(new Text(key), new IntWritable(sum));
		}
	}
	
	// Custom partitioner class.
	public static class DevPartitioner extends
	Partitioner < Text, IntWritable >
	{
		@Override
		public int getPartition(Text key, IntWritable value, int numReduceTasks) {
			// No need to do anything if there's no reducers running.
			if(numReduceTasks == 0)
			{
				return 0;
			}

			// Split records on the mod. numReduceTasks of the first letter of the text key.
			int ind;
			ind = key.charAt(0);
			return ind % numReduceTasks;
		}
	}
   
	@Override
	public int run(String[] arg) throws Exception
	{
		Job job = new Job();
		job.setJarByClass(HW1.class);
		job.setJobName("HW Task 1");

		FileInputFormat.setInputPaths(job, new Path(arg[0]));
		FileOutputFormat.setOutputPath(job,new Path(arg[1]));

		job.setMapperClass(MapClass.class);
		// We'll set up a combiner in addition to a reducer.
		// Due to specifics of the task, both of these functions can be fulfilled by a single class.
		job.setCombinerClass(ReduceClass.class);
		job.setReducerClass(ReduceClass.class);
		job.setPartitionerClass(DevPartitioner.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);

		// Directly set the amount of reduce tasks to run.
		job.setNumReduceTasks(2);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		// We're expecting a filename with key-value pair for city names.
		DistributedCache.addCacheFile(new Path(arg[2]).toUri(), job.getConfiguration());

		System.exit(job.waitForCompletion(true)? 0 : 1);
		return 0;
	}
   
	public static void main(String[] args) throws Exception{
		int exitCode = ToolRunner.run(new HW1(), args);
		System.exit(exitCode);
	}
}
