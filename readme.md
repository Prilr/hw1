OS: CentOS 7 on Docker, Windows host
Required software: JDK 1.7.0, Hadoop 2.7.0, Maven 3.5.4, JUnit 3.8.1

Sequence of actions:
	* Unzip the input data using the unzip.bat script
	* Launch the docker container using the docker_launch.bat script
	* Upload the input files, sources and scripts using the host_upload.bat script
	* Enter the container terminal
	* Create HDFS directories using the hdfs_dirs.sh script
	* Load input data into HDFS using the load.sh script
	* Build the JAR file using the build.sh script
	* Launch the job using the launch.sh script
	* View reducer outputs using the view.sh script with the number of the reducer as an argument