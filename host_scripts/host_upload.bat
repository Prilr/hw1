docker ps --filter status=running --format "{{.ID}}" > temp.txt
set /p DOCKER_ID=<temp.txt
rm temp.txt

docker cp ../docker_scripts %DOCKER_ID%:/home/hw1
docker cp ../src %DOCKER_ID%:/home/hw1/src
docker cp data %DOCKER_ID%:/home/hw1/data